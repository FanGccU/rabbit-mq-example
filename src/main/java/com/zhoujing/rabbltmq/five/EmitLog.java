package com.zhoujing.rabbltmq.five;

import com.rabbitmq.client.Channel;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/17-21:00-星期日
 *
 * 生产者，fanout模式
 * 发送消息给交互机
 */
public class EmitLog {

    public static final String EXCHANGE_NAME = "logs";

    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();

        Scanner input = new Scanner(System.in);
        while (input.hasNext()) {
            String message = input.next();
            /*
            * basicPublish(交互机名称,路由Key,其他参数,消息)
            * 因为之前没有接触到交换机所以之前一直是为空字符串的（默认交互机）
            * 因为消费者那边创建的交互机并没有设置路由Key所以这里也不进行设置
            * */
            channel.basicPublish(EXCHANGE_NAME,"12",null,message.getBytes("UTF-8"));
            System.out.println("生产者发送的消息为：" + message);
        }
    }
}
