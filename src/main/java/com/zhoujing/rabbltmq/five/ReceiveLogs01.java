package com.zhoujing.rabbltmq.five;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/17-20:32-星期日
 * <p>
 * 消费者01
 */
public class ReceiveLogs01 {

    /**
     * 交互机名称
     */
    public static final String EXCHANGE_NAME = "logs";

    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();
        /*
         * 声明一个交换机
         * exchangeDeclare(交换机名称,交换机类型);
         * */
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        /*
         * 声明一个临时的队列
         * 临时队列名称为随机
         * 当消费者断开与队列连接的时候，队列就会删除
         * */
        String queueName = channel.queueDeclare().getQueue();
        /*
         * 交互机与队列进行绑定
         * exchangeBind(队列名称,交换机名称,路由Key);
         * */
        channel.queueBind(queueName, EXCHANGE_NAME, "123456");
        System.out.println("消费者01，等待接收消息，把接收的消息打印到屏幕上……");

        // 接收消息
        DeliverCallback deliverCallback = (tag, message) -> {
            System.out.println("接收到消息为："+new String(message.getBody(),"UTF-8"));
        };

        // 取消接收
        CancelCallback cancelCallback = (tag) -> {
            System.out.println("用户取消消息的接收，消息的标签为："+tag);
        };
        // 接收消息
        channel.basicConsume(queueName, true, deliverCallback,cancelCallback);

    }
}
