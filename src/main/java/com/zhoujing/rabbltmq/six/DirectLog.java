package com.zhoujing.rabbltmq.six;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/18-14:32-星期一
 * <p>
 * 生产者，direct模式
 * 发送消息
 */
public class DirectLog {

    /**
     * 交换机名称
     */
    public static final String EXCHANGE_NAME = "DirectLog";
    /**
     * 队列01名称
     */
    public static final String QUEUE_MAME01 = "ReceiveLogs01";
    /**
     * 队列02名称
     */
    public static final String QUEUE_MAME02 = "ReceiveLogs02";

    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();
        // 声明交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        // 交换机与队列进行绑定
        channel.queueBind(QUEUE_MAME01, EXCHANGE_NAME, "123");
        channel.queueBind(QUEUE_MAME02, EXCHANGE_NAME, "321");
        Scanner input = new Scanner(System.in);
        int count = 0;
        String routingKey = "123";
        while (input.hasNext()) {
            count++;
            if (count % 2 == 0) {
                routingKey = "123";
            } else {
                routingKey = "321";
            }
            String message = input.next();
            // 123是ReceiveLogs01的路由Key，只有ReceiveLogs01才能接收到消息
            channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes("UTF-8"));
            System.out.println("发送的消息为：" + message);
        }
    }
}
