package com.zhoujing.rabbltmq.three;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/9-15:31-星期六
 * <p>
 * 消息在手动应答时是不丢失的，放回队列中重新消费
 */
public class Task02 {

    /**
     * 队列名称
     */
    public static final String TASK_QUEUE_NAME = "ack_queue";

    /**
     * 生产者发送消息
     * @param args
     * @throws IOException
     * @throws TimeoutException
     */
    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();
        // 开启发布确认
        channel.confirmSelect();
        // 声明队列
        //queueDeclare(队列名称，是否持久化，是否共享，是否删除，其他参数);
        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
        // 获取控制台消息
        Scanner input = new Scanner(System.in);
        // 判断是否有下一个消息
        while (input.hasNext()) {
            String message = input.next();
            //basicPublish(交互机名称空字符串为默认，队里名称，其他参数，消息必须为二进制)
            channel.basicPublish("", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes("UTF-8"));
            System.out.println("发送的消息为：" + message);
        }
    }

}
