package com.zhoujing.rabbltmq.seven;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.zhoujing.rabbltmq.six.DirectLog;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/18-14:53-星期一
 *
 * 消费者01
 */
public class ReceiveLogs01 {

    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();
        // 声明队列
        channel.queueDeclare(TopictLog.QUEUE_NAME[0],false,false,false,null);

        DeliverCallback deliverCallback = (tag,message)->{
            System.out.println(tag+"接收到的消息为："+new String(message.getBody(),"UTF-8"));
            System.out.println("routingKey为："+message.getEnvelope().getRoutingKey());
        };

        CancelCallback cancelCallback = (tag)->{
            System.out.println(tag + "取消了消息的回调");
        };

        System.out.println("ReceiveLogs01正在等待消息……");
        // 接收消息
        channel.basicConsume(TopictLog.QUEUE_NAME[0],true,deliverCallback,cancelCallback);
    }
}
