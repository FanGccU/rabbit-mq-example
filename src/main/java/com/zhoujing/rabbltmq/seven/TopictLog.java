package com.zhoujing.rabbltmq.seven;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/18-17:32-星期一
 * <p>
 * 生产者
 * 主题类型
 */
public class TopictLog {

    /**
     * 交换机名称
     */
    public static final String EXCHANGE_NAME = "TopictLog";
    /**
     * 队列名称
     */
    public static final String[] QUEUE_NAME = {"Queue01","Queue02","Queue03"};

    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();
        // 声明交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
        // 交换机与队列进行绑定
        channel.queueBind(QUEUE_NAME[0], EXCHANGE_NAME, "*.orange.*");
        channel.queueBind(QUEUE_NAME[1], EXCHANGE_NAME, "*.*.rabbit");
        channel.queueBind(QUEUE_NAME[2], EXCHANGE_NAME, "laz.#");

        // 绑定数据
        List<String> routingKey = new ArrayList<>();
        routingKey.add("test.orange.end");
        routingKey.add("hello.world.rabbit");
        routingKey.add("laz.yes.yes.ok.end");
        routingKey.add("hello.java.yes");
        routingKey.add("hello.world");
        routingKey.add("laz.end");

        for (String key : routingKey) {
            String message = key;
            channel.basicPublish(EXCHANGE_NAME,key,null,message.getBytes("UTF-8"));
        }
    }

}
