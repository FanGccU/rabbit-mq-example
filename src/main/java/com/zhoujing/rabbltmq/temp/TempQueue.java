package com.zhoujing.rabbltmq.temp;

import com.rabbitmq.client.Channel;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/17-16:41-星期日
 *
 * 临时队列
 */
public class TempQueue {
    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();
        channel.confirmSelect();
        String tempQueue = channel.queueDeclare().getQueue();
        System.out.println("临时队列名称为："+tempQueue);
    }
}
