package com.zhoujing.rabbltmq.one;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/6/30-21:00-星期四
 *
 * 消费者发送消息
 */
public class Producer {

    /**
     * 队列名称
     */
    public static final String QUEUE_NAME = "hello";

    /**
     * 发送消息
     * @param args
     */
    public static void main(String[] args) throws IOException, TimeoutException {
        // 创建一个连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 工厂IP连接RabbitMQ的队列
        factory.setHost("kaibox.top");
        // 15672是web访问的端口
        factory.setPort(5672);
        // 设置超时时间
        factory.setHandshakeTimeout(60000);
        // 用户名
        factory.setUsername("admin");
        // 设置密码
        factory.setPassword("admin");
        // 建立连接
        Connection connection = factory.newConnection();
        // 获取信道
        Channel channel = connection.createChannel();

        /*
        * 生成一个队列
        * 1、队列名称
        * 2、队列里面的消息是否持久化（磁盘），默认情况消息存储到内存中，true存储到磁盘中，false存储到内存当中
        * 3、该队列是否只供一个消费者进行消费。是否进行消息共享，true只供一个，false多个消费者
        * 4、是否自动删除，最后一个消费者断开连接后，该队一句是否自动删除，true自动删除，false不自动删除
        * 5、其他
        * */
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        // 消息
        String message = "hello world!";


        /*
        * 发送一个消费
        * 1、发送到哪个交换机
        * 2、路由的Key值是哪个，本次是队列的名称
        * 3、其他参数信息
        * 4、发送消息的消息体，消息体只能是二进制进行传输的
        * */
        channel.basicPublish("",QUEUE_NAME,null,message.getBytes());

        System.out.println("消息发送完毕");
    }

}
