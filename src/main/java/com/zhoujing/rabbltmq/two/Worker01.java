package com.zhoujing.rabbltmq.two;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/1-22:40-星期五
 *
 * 工作线程
 */
public class Worker01 {

    private static final String QUEUE_NAME = "hello";

    public static void main(String[] args) throws IOException, TimeoutException {

        DeliverCallback deliverCallback = (consumerTag,message)->{
            System.out.println(consumerTag+"接收到的消息："+new String(message.getBody()));
        };

        CancelCallback cancelCallback = consumerTag->{
            System.out.println(consumerTag+"：取消回调");
        };

        System.out.println("C2正在等待……");
        Channel channel = RabbitMqUtils.getChannel();
        channel.basicConsume(QUEUE_NAME,true,deliverCallback,cancelCallback);
    }
}
