package com.zhoujing.rabbltmq.two;

import com.rabbitmq.client.Channel;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/3-13:37-星期日
 * <p>
 * 生产者，发送大量的消息
 */
public class Task01 {

    public static final String QUEUE_NAME = "hello";

    /**
     * 发送大量的信息
     *
     * @param args
     */
    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();
        // 因为消费者有多个所以第三个参数必须为false
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        Scanner input = new Scanner(System.in);
        while (input.hasNext()) {
            String message = input.next();
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
            System.out.println("消息发送完成："+message);
        }
    }
}
