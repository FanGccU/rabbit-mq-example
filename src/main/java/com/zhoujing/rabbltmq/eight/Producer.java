package com.zhoujing.rabbltmq.eight;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/18-23:19-星期一
 * <p>
 * 生产者
 */
public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();
        // String message = "Hello RabbitMQ!";
        // 消息有效时间为10秒，超出将进入死信队列
        // AMQP.BasicProperties properties = new AMQP.BasicProperties().builder().expiration("10000").build();
        // channel.basicPublish(Broker.NORMAL_EXCHANGE,"normal",null,message.getBytes("UTF-8"));
        for (int i = 0; i < 10; i++) {
            String message = i+"";
            channel.basicPublish(Broker.NORMAL_EXCHANGE,"normal",null,message.getBytes("UTF-8"));
        }
    }
}
