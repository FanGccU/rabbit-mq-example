package com.zhoujing.rabbltmq.eight;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/18-22:22-星期一
 *
 * 消费者01
 */
public class Consumer01 {

    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();

        final String FIVE = "5";
        DeliverCallback deliverCallback = (tag,message)->{
            String msg = new String(message.getBody(),"UTF-8");
            if(FIVE.equals(msg)){
                // 拒绝消息
                // basicReject(消息标签,是否重新放回普通队列);
                channel.basicReject(message.getEnvelope().getDeliveryTag(),false);
            }else{
                System.out.println(tag + "接收到的消息为：" + new String(message.getBody(),"UTF-8"));
                // 手动确认
                // basicAck(消息标签,是否是批量);
                channel.basicAck(message.getEnvelope().getDeliveryTag(),false);
            }
        };

        CancelCallback cancelCallback = (tag)->{
            System.out.println(tag+"取消了消息回调");
        };
        System.out.println("消费者01正在等待消息接收……");
        channel.basicConsume(Broker.NORMAL_QUEUE,false,deliverCallback,cancelCallback);
    }

}
