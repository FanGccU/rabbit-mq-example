package com.zhoujing.rabbltmq.eight;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/18-22:45-星期一
 *
 * 声明交换机和队列
 */
public class Broker {

    /**
     * 正常交换机
     */
    public static final String NORMAL_EXCHANGE = "normal_exchange";
    /**
     * 死信交换机
     */
    public static final String DEAD_EXCHANGE = "dead_exchange";
    /**
     * 正常队列
     */
    public static final String NORMAL_QUEUE = "normal_queue";
    /**
     * 死信队列
     */
    public static final String DEAD_QUEUE = "dead_queue";

    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();
        // 声明正常交换机
        channel.exchangeDeclare(NORMAL_EXCHANGE, BuiltinExchangeType.DIRECT);
        // 声明死信交换机
        channel.exchangeDeclare(DEAD_EXCHANGE, BuiltinExchangeType.DIRECT);

        Map<String,Object> arguments = new HashMap<>();
        // 正常队列对应的死信队列
        arguments.put("x-dead-letter-exchange",DEAD_EXCHANGE);
        // 设置死信 RoutingKey
        arguments.put("x-dead-letter-routing-key","dead");
        // 设置过期时间，一般由生产者设置过期时间
        // arguments.put("x-message-ttl",10000);
        // 设置队列最大只能存储6个消息
        // arguments.put("x-max-length",6);
        // 声明正常队列
        channel.queueDeclare(NORMAL_QUEUE,false,false,false,arguments);
        // 声明死信队列
        channel.queueDeclare(DEAD_QUEUE,false,false,false,null);

        /*
        * 交换机与队列进行绑定
        * */
        channel.queueBind(NORMAL_QUEUE,NORMAL_EXCHANGE,"normal");
        channel.queueBind(DEAD_QUEUE,DEAD_EXCHANGE,"dead");
    }
}
