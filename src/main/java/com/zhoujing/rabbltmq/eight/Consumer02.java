package com.zhoujing.rabbltmq.eight;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.zhoujing.rabbltmq.utils.RabbitMqUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/18-22:22-星期一
 *
 * 消费者01
 */
public class Consumer02 {

    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMqUtils.getChannel();

        DeliverCallback deliverCallback = (tag,message)->{
            System.out.println(tag + "接收到的消息为：" + new String(message.getBody(),"UTF-8"));
        };

        CancelCallback cancelCallback = (tag)->{
            System.out.println(tag+"取消了消息回调");
        };
        System.out.println("消费者02正在等待消息接收……");
        channel.basicConsume(Broker.DEAD_QUEUE,true,deliverCallback,cancelCallback);
    }

}
