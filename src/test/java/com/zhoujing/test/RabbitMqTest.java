package com.zhoujing.test;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * @author 周敬
 * @version 1.0
 * @createTime 2022/7/16-21:32-星期六
 *
 * 测试
 */
public class RabbitMqTest {

    @Test
    public void concurrentNavigableMapTest(){
        ConcurrentNavigableMap map = new ConcurrentSkipListMap();
        map.put("1","noe");
        map.put("2","two");
        map.put("3","three");
        map.put("4","four");
        map.put("5","five");

        // headMap（T toKey）方法返回小于给定键的键的映射视图。
        ConcurrentNavigableMap headMap = map.headMap("3",true);
        headMap.remove("1");
//        Set<String> keySet = headMap.keySet();
//        keySet.forEach(key -> System.out.println("key："+key+"，value："+headMap.get(key)));
        System.out.println(map);
        System.out.println(headMap);
    }

    @Test
    public void synchronizedSortedSet(){
        SortedSet<String> sortedSet = new TreeSet<>();
        sortedSet.add("B");
        sortedSet.add("D");
        sortedSet.add("A");
        sortedSet.add("C");
        System.out.println(sortedSet);
        // synchronizedSortedSet()方法用于返回由指定排序集支持的同步(线程安全)排序集。
        SortedSet<String> synchronizedSortedSet = Collections.synchronizedSortedSet(sortedSet);
        System.out.println(synchronizedSortedSet);
    }

    @Test
    public void remainder(){
        for (int i = 0; i < 50; i++) {
            if(i%2 == 0){
                System.out.println(i);
            }
        }
    }


}
